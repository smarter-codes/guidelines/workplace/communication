# Communication

## Asynchronous
They are [written first](https://about.gitlab.com/company/culture/all-remote/asynchronous/#how-to-decline-meetings-in-favor-of-async).
They [document everything](https://about.gitlab.com/company/culture/all-remote/asynchronous/#documentation-as-a-prerequisite) for others to read later, ranging from WHAT they are doing, WHY they are doing, HOW they are doing it.
They often cite documentations during their conversations, so that important documentations get seen by colleagues as much as possible. ~Async


## Thorough. Yet to the point
They write assuming others [do not know anything of the context](https://about.gitlab.com/company/culture/all-remote/effective-communication/#understanding-low-context-communication).
So they write thoroughly. Yet they write such that others get to learn the topic ‘as soon as possible’ - so they are also to the point. ~Thorough


## Fun
They encourage you. They strike jokes. They give sensitive feedback to colleagues - which is received well by them. ~Fun

## International Exposure
They are able to understand most ascents of the world. They are punctual. They present themselves well over video calls. ~Exposure

# See Also
[Communicating in Engineering teams](https://gitlab.com/smarter-codes/guidelines/software-engineering/communication)
